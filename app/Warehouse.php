<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Warehouse extends Model
{
    public function company_office()
    {
    	return $this->belongsTo(CompanyOffice::class);
    }
}
