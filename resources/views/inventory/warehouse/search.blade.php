@extends('layouts.app')

@section('content')

	<div class="container-fluid bg-white shadow-box">
		<form id="form-ware-house-search" action="/bodegas" accept-charset="UTF-8" method="get">
			<input name="utf8" type="hidden" value="&#x2713;" />
			<div class="row mt-10">
				<div class="col-md-2 col-xs-12 text-left">
		    		<a class="btn btn-success btn-accion btn-sm" id="btn-new-ware-house" data-toggle="tooltip" data-placement="bottom" data-html="true" href="/bodegas/new">
		  				<i class="fa fa-plus"></i> Nuevo
					</a>
				</div>
				<div class="col-md-7 col-xs-12 text-center">
					<label>Mostrar:</label>
					<div class="btn-group" data-toggle="buttons">
						<label class="btn btn-default btn-xs active">
							<input type="radio" name="ware_house_status_query" id="ware_house_status_query_true" value="true" onchange="$(&#39;#form-ware-house-search&#39;).submit();" checked="checked" /> Vigente
						</label>
						<label class="btn btn-default btn-xs">
							<input type="radio" name="ware_house_status_query" id="ware_house_status_query_false" value="false" onchange="$(&#39;#form-ware-house-search&#39;).submit();" /> No vigente
						</label>
						<label class="btn btn-default btn-xs">
							<input type="radio" name="ware_house_status_query" id="ware_house_status_query_all" value="all" onchange="$(&#39;#form-ware-house-search&#39;).submit();" />
							Todos
						</label>
					</div>
				</div>
				<div class="col-md-3 col-xs-12 text-right">
					<div class="form-search">
						<input type="text" name="query" id="navbar-search" class="form-control" placeholder="Buscar" data-toggle="tooltip" data-placement="bottom" data-title="Buscar por nombre o dirección" />
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12 col-xs-12 text-right">
					<label>Descargar</label>
					<label>
						<a data-toggle="tooltip" data-placement="bottom" data-html="true" data-title="Descargar consulta &lt;br&gt;en formato &lt;br&gt;Excel." href="/bodegas.xls?ware_house_status_query=true">
							Excel
						</a>					
					</label>
				</div>
			</div>
		</form>
			<div class="row mt-10">
			<div class="col-md-12">
			  <div id="warehouses-list">
		  		<div class="table-responsive">
		    		<table class="table table-hover">
		      			<thead>
				            <tr>
				              <th>Sucursal</th>
				              <th>Nombre</th>
				              <th>Dirección</th>
				              <th>Estado</th>
				              <th class="align-right">Acciones</th>
				            </tr>
		        		<tbody>
						<tr id="warehouse-13">
							<td>
							  Casa matriz
							</td>
							<td>
							  Bodega principal
							</td>
							<td>
							  
							</td>
							<td>
							  <span class="label warehouse-status-true">Vigente</span>
							</td>
							<td class="align-right width-150">
							  <div class="table-actions-toolbar width-150">
							    <div class="btn-group">
							        <a class="btn btn-default btn-xs" data-toggle="tooltip" data-title="Editar" data-container="body" href="/bodegas/13/edit">
							          <i class="fa fa-fw fa-pencil"></i>
									</a>
								</div>
							  </div>
							</td>
						</tr>
						<tr id="warehouse-77">
							<td>
							  Casa matriz
							</td>
							<td>
							  Bodega secundaria
							</td>
							<td>
							  Las Condes 2828
							</td>
							<td>
							  <span class="label warehouse-status-true">Vigente</span>
							</td>
							<td class="align-right width-150">
							  <div class="table-actions-toolbar width-150">
							    <div class="btn-group">
							        <a class="btn btn-default btn-xs" data-toggle="tooltip" data-title="Editar" data-container="body" href="/bodegas/77/edit">
							          <i class="fa fa-fw fa-pencil"></i>
									</a>
								</div>
							  </div>
							</td>
						</tr>
		    		</table>
		  		</div>
				<div id="warehouse-paginate" class="text-center">

				</div>
				</div>
		</div>
	</div>

@endsection