@extends('layouts.app')

@section('content')
<div class="container-fluid bg-white shadow-box pt-20">
  <form id="warehouse-form" class="form-horizontal" enctype="multipart/form-data" action="/bodegas" accept-charset="UTF-8" method="post">
  	<input name="utf8" type="hidden" value="&#x2713;" />
    <div class="row mt-10">
      <div class="col-md-6 col-md-offset-3">
        <div class="form-group">
          <div class="col-md-4">
            <label>Sucursal <abbr title="Campo obligatorio">*</abbr></label>
          </div>
          <div class="col-md-8">
            <select class="filter-select form-control" name="ware_house[branch_id]" id="ware_house_branch_id">
            	<option value="">Seleccione...</option>
				<option selected="selected" value="4">Casa matriz</option>
			</select>
          </div>
        </div>
        <div class="form-group">
          <div class="col-md-4">
            <label>Nombre <abbr title="Campo obligatorio">*</abbr></label>
          </div>
          <div class="col-md-8">
            <input class="form-control" placeholder="Ingrese nombre de la bodega" type="text" name="ware_house[name]" id="ware_house_name" />
          </div>
        </div>
        <div class="form-group">
          <div class="col-md-4">
            <label>Dirección</label>
          </div>
          <div class="col-md-8">
            <input class="form-control" placeholder="Ingrese dirección" type="text" name="ware_house[address]" id="ware_house_address" />
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-6 col-md-offset-3 text-right mb-20">
        <a class="btn btn-link" href="/bodegas">Cancelar</a>
      </div>
    </div>
	</form>
</div>

@endsection