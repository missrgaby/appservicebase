@extends('layouts.app')

@section('content')

<form id="form-filter" data-turboboost="true" action="/stock-actual" accept-charset="UTF-8" data-remote="true" method="get"><input name="utf8" type="hidden" value="&#x2713;" />
  <div class="row mt-10">
    <div class="col-md-12">
      <div class="container-fluid bg-white shadow-box">
        <div class="row mt-10">
          <div class="col-md-1">
            <label>Bodega(s)</label>
          </div>
          <div class="col-md-5">
            <div class="form-group mb-10">
              <select name="ware_house_ids[]" id="ware_house_ids" multiple="multiple" class="select2 form-control"><option value="13">[Casa matriz] Bodega principal</option>
<option value="77">[Casa matriz] Bodega secundaria</option></select>
            </div>
          </div>
          <div class="col-md-1">
            <label>Categoría(s)</label>
          </div>
          <div class="col-md-5">
            <select name="categories_ids[]" id="categories_ids" multiple="multiple" class="select2 form-control"><option value="341">accesorios</option>
<option value="25">Camisa</option>
<option value="13">Hardware</option>
<option value="15">Otros</option>
<option value="306">Pantalones</option>
<option value="12">Servicios</option>
<option value="20">Sin categoría</option>
<option value="14">Software</option>
<option value="24">Zapatos</option></select>
          </div>
        </div>
        <div class="row mb-10">
          <div class="col-md-1">
            <label>Producto(s)</label>
          </div>
          <div class="col-md-5">
            <select name="product_ids[]" id="product_ids" multiple="multiple" class="filter-select-product-inventory form-control"><option value="571">[H-ALA3-335272] Adaptador Lightning a 30-pin</option>
<option value="121">[03] Asesorías profesionales</option>
<option value="572">[C-CEAP-45173] Camisa en algodón premium</option>
<option value="13568">[01] Capacitación y Asesoría </option>
<option value="12213">[] Corrige Dato Receptor</option>
<option value="115">[14554] Disco Externo 2.5&quot; USB 3.0</option>
<option value="118">[234234] Epson Scanner Perfection V19</option>
<option value="122">[11312] Licencia Win10 xp 2017</option>
<option value="120">[02] Mantención de aplicaciones web</option>
<option value="19506">[SC-M-962251] Marraquetas</option>
<option value="117">[MO3321] Mouse MS-117C Óptico USB Negro</option>
<option value="113">[24825] Multifuncional Laser B&amp;N WiFi/Ethernet </option>
<option value="112">[PT12322] Pantalla LCD Touch 7</option>
<option value="19564">[P-PR-920711] pantalon rayas</option>
<option value="19566">[P-PR-PRB-149490] pantalon rayas pantalon rayas blancas</option>
<option value="19565">[P-PR-PRN-411544] pantalon rayas pantalon rayas negras</option>
<option value="19567">[P-PR-PRR-540227] pantalon rayas pantalon rayas rojas</option>
<option value="114">[PEN123112] Pendrive 16GB USB 2.0 JumpDrive S50</option>
<option value="116">[54322] SmartTV UJ6300 43&quot; LED Ultra HD 4K </option>
<option value="569">[518342] Zapatilla Agility Peak Flex Talla 41</option>
<option value="570">[584030] Zapatilla Agility Peak Flex Talla 42</option>
<option value="575">[C-CEAP-TL-155728] Camisa en algodón premium Talla L</option>
<option value="574">[C-CEAP-TM-675108] Camisa en algodón premium Talla M</option>
<option value="573">[C-CEAP-TS-738937] Camisa en algodón premium Talla S</option>
<option value="576">[C-CEAP-TX-925068] Camisa en algodón premium Talla XL</option>
<option value="568">[425085] Zapatilla Agility Peak Flex</option></select>
          </div>
          <div class="col-md-1">
            <label data-toggle="tooltip" data-title="Buscar stock disponible desde" data-container="body">Stock <i class="fa fa-question-circle-o"></i></label>
          </div>
          <div class="col-md-2">
            <input type="text" name="stock_search_from" id="stock_search_from" class="form-control" />
          </div>
          <div class="col-md-1">
            <label>Hasta</label>
          </div>
          <div class="col-md-2">
            <input type="text" name="stock_search_to" id="stock_search_to" class="form-control" />
          </div>
        </div>
      </div>
    </div>
  </div>
</form>
<div class="row mt-10 mt-10">
  <div class="col-md-12">
    <div class="container-fluid bg-white shadow-box">
      <div id="inventories-details">
        <div class="row mt-10 mb-10">
  <div class="col-md-offset-6 col-md-4 col-xs-12">
    <span class="filter-spinner"></span> <!-- Spinner de espera! -->
  </div>
  <div class="col-md-2 col-xs-12 text-right">
    <label class="mb-0">Descargar </label>
    <label class="mb-0">
      <a data-toggle="tooltip" data-placement="bottom" data-html="true" data-title="Descargar consulta en formato excel" href="/stock-actual.xls">
        Excel
</a>    </label>
  </div>
</div>
<div class="row mt-10">
  <div class="col-md-offset-4 col-md-4 col-xs-12">
    <div class="box">
      <div class="icon">
        <div class="info">
          <p class="title">Total stock disponible</p>
          <h5 class="title">346</h5>
        </div>
      </div>
    </div>
  </div>
  <div class="col-md-4 col-xs-12">
    <div class="box">
      <div class="icon">
        <div class="info">
          <p class="title">Valor del inventario</p>
          <h5 class="title">$12.591.730</h5>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="row">
  <div class="col-sx-12 col-sm-12 col-lg-12 mt-20">
        <div class="table-responsive">
          <table class="table">
            <thead>
              <tr>
                <th>Categoría</th>
                <th>Producto</th>
                <th>Estado</th>
                <th>Stock disponible</th>
                <th>Costo promedio</th>
                <th>Total</th>
                <th>Acciones</th>
              </tr>
              <tbody>
                  <tr>
                    <td>
                      <span data-toggle="tooltip" data-placement="bottom" title="Hardware">
                        Hardware
                      </span>                    </td>
                    <td>
                      <span data-toggle="tooltip" data-placement="bottom" title="Epson Scanner Perfection V19">
                        Epson Scanner Perfection V19
                      </span>
                      <p class="bottom">
                        234234
                      </p>
                    </td>
                    <td>
                      <span class="label label-success">Vigente</span>
                    </td>
                    <td>
                      119
                    </td>
                    <td>
                      $55.000
                    </td>
                    <td>
                      $6.545.000
                    </td>
                    <td>
                      <div class="table-actions-toolbar">
                        <div class="btn-group">
                          <a class="btn btn-default btn-xs" data-toggle="tooltip" data-title="Ver tarjeta de existencia" data-container="body" href="/stock-actual/118/existencia">
                            <i class="fa fa-fw fa-bars"></i>
</a>
                          <a class="btn btn-default btn-xs" data-modal="stock-by-warehouses" data-url="/stock-actual/118/stock-por-bodega" data-toggle="tooltip" data-title="Ver disponibilidad por bodega" data-container="body" href="#">
                            <i class="fa fa-fw fa-map-marker"></i>
</a>                        </div>
                      </div>
                    </td>
                  </tr>
                  <tr>
                    <td>
                      <span data-toggle="tooltip" data-placement="bottom" title="Hardware">
                        Hardware
                      </span>                    </td>
                    <td>
                      <span data-toggle="tooltip" data-placement="bottom" title="Disco Externo 2.5&quot; USB 3.0">
                        Disco Externo 2.5&quot; USB 3.0
                      </span>
                      <p class="bottom">
                        14554
                      </p>
                    </td>
                    <td>
                      <span class="label label-success">Vigente</span>
                    </td>
                    <td>
                      116
                    </td>
                    <td>
                      $20.000
                    </td>
                    <td>
                      $2.320.000
                    </td>
                    <td>
                      <div class="table-actions-toolbar">
                        <div class="btn-group">
                          <a class="btn btn-default btn-xs" data-toggle="tooltip" data-title="Ver tarjeta de existencia" data-container="body" href="/stock-actual/115/existencia">
                            <i class="fa fa-fw fa-bars"></i>
</a>
                          <a class="btn btn-default btn-xs" data-modal="stock-by-warehouses" data-url="/stock-actual/115/stock-por-bodega" data-toggle="tooltip" data-title="Ver disponibilidad por bodega" data-container="body" href="#">
                            <i class="fa fa-fw fa-map-marker"></i>
</a>                        </div>
                      </div>
                    </td>
                  </tr>
                  <tr>
                    <td>
                      <span data-toggle="tooltip" data-placement="bottom" title="Hardware">
                        Hardware
                      </span>                    </td>
                    <td>
                      <span data-toggle="tooltip" data-placement="bottom" title="Mouse MS-117C Óptico USB Negro">
                        Mouse MS-117C Óptico USB Negro
                      </span>
                      <p class="bottom">
                        MO3321
                      </p>
                    </td>
                    <td>
                      <span class="label label-success">Vigente</span>
                    </td>
                    <td>
                      27
                    </td>
                    <td>
                      $9.990
                    </td>
                    <td>
                      $269.730
                    </td>
                    <td>
                      <div class="table-actions-toolbar">
                        <div class="btn-group">
                          <a class="btn btn-default btn-xs" data-toggle="tooltip" data-title="Ver tarjeta de existencia" data-container="body" href="/stock-actual/117/existencia">
                            <i class="fa fa-fw fa-bars"></i>
</a>
                          <a class="btn btn-default btn-xs" data-modal="stock-by-warehouses" data-url="/stock-actual/117/stock-por-bodega" data-toggle="tooltip" data-title="Ver disponibilidad por bodega" data-container="body" href="#">
                            <i class="fa fa-fw fa-map-marker"></i>
</a>                        </div>
                      </div>
                    </td>
                  </tr>
                  <tr>
                    <td>
                      <span data-toggle="tooltip" data-placement="bottom" title="Camisa">
                        Camisa
                      </span>                    </td>
                    <td>
                      <span data-toggle="tooltip" data-placement="bottom" title="Camisa en algodón premium">
                        Camisa en algodón premium
                      </span>
                      <p class="bottom">
                        C-CEAP-45173
                      </p>
                    </td>
                    <td>
                      <span class="label label-success">Vigente</span>
                    </td>
                    <td>
                      22
                    </td>
                    <td>
                      $5.500
                    </td>
                    <td>
                      $121.000
                    </td>
                    <td>
                      <div class="table-actions-toolbar">
                        <div class="btn-group">
                          <a class="btn btn-default btn-xs" data-toggle="tooltip" data-title="Ver tarjeta de existencia" data-container="body" href="/stock-actual/572/existencia">
                            <i class="fa fa-fw fa-bars"></i>
</a>
                          <a class="btn btn-default btn-xs" data-modal="stock-by-warehouses" data-url="/stock-actual/572/stock-por-bodega" data-toggle="tooltip" data-title="Ver disponibilidad por bodega" data-container="body" href="#">
                            <i class="fa fa-fw fa-map-marker"></i>
</a>                        </div>
                      </div>
                    </td>
                  </tr>
                  <tr>
                    <td>
                      <span data-toggle="tooltip" data-placement="bottom" title="Zapatos">
                        Zapatos
                      </span>                    </td>
                    <td>
                      <span data-toggle="tooltip" data-placement="bottom" title="Zapatilla Agility Peak Flex Talla 42">
                        Zapatilla Agility Peak Flex Talla 42
                      </span>
                      <p class="bottom">
                        584030
                      </p>
                    </td>
                    <td>
                      <span class="label label-success">Vigente</span>
                    </td>
                    <td>
                      20
                    </td>
                    <td>
                      $15.000
                    </td>
                    <td>
                      $300.000
                    </td>
                    <td>
                      <div class="table-actions-toolbar">
                        <div class="btn-group">
                          <a class="btn btn-default btn-xs" data-toggle="tooltip" data-title="Ver tarjeta de existencia" data-container="body" href="/stock-actual/570/existencia">
                            <i class="fa fa-fw fa-bars"></i>
</a>
                          <a class="btn btn-default btn-xs" data-modal="stock-by-warehouses" data-url="/stock-actual/570/stock-por-bodega" data-toggle="tooltip" data-title="Ver disponibilidad por bodega" data-container="body" href="#">
                            <i class="fa fa-fw fa-map-marker"></i>
</a>                        </div>
                      </div>
                    </td>
                  </tr>
                  <tr>
                    <td>
                      <span data-toggle="tooltip" data-placement="bottom" title="Hardware">
                        Hardware
                      </span>                    </td>
                    <td>
                      <span data-toggle="tooltip" data-placement="bottom" title="Multifuncional Laser B&amp;N WiFi/Ethernet ">
                        Multifuncional Laser B&amp;N WiFi/Ethernet 
                      </span>
                      <p class="bottom">
                        24825
                      </p>
                    </td>
                    <td>
                      <span class="label label-success">Vigente</span>
                    </td>
                    <td>
                      17
                    </td>
                    <td>
                      $45.000
                    </td>
                    <td>
                      $765.000
                    </td>
                    <td>
                      <div class="table-actions-toolbar">
                        <div class="btn-group">
                          <a class="btn btn-default btn-xs" data-toggle="tooltip" data-title="Ver tarjeta de existencia" data-container="body" href="/stock-actual/113/existencia">
                            <i class="fa fa-fw fa-bars"></i>
</a>
                          <a class="btn btn-default btn-xs" data-modal="stock-by-warehouses" data-url="/stock-actual/113/stock-por-bodega" data-toggle="tooltip" data-title="Ver disponibilidad por bodega" data-container="body" href="#">
                            <i class="fa fa-fw fa-map-marker"></i>
</a>                        </div>
                      </div>
                    </td>
                  </tr>
                  <tr>
                    <td>
                      <span data-toggle="tooltip" data-placement="bottom" title="Zapatos">
                        Zapatos
                      </span>                    </td>
                    <td>
                      <span data-toggle="tooltip" data-placement="bottom" title="Zapatilla Agility Peak Flex Talla 41">
                        Zapatilla Agility Peak Flex Talla 41
                      </span>
                      <p class="bottom">
                        518342
                      </p>
                    </td>
                    <td>
                      <span class="label label-success">Vigente</span>
                    </td>
                    <td>
                      12
                    </td>
                    <td>
                      $18.000
                    </td>
                    <td>
                      $216.000
                    </td>
                    <td>
                      <div class="table-actions-toolbar">
                        <div class="btn-group">
                          <a class="btn btn-default btn-xs" data-toggle="tooltip" data-title="Ver tarjeta de existencia" data-container="body" href="/stock-actual/569/existencia">
                            <i class="fa fa-fw fa-bars"></i>
</a>
                          <a class="btn btn-default btn-xs" data-modal="stock-by-warehouses" data-url="/stock-actual/569/stock-por-bodega" data-toggle="tooltip" data-title="Ver disponibilidad por bodega" data-container="body" href="#">
                            <i class="fa fa-fw fa-map-marker"></i>
</a>                        </div>
                      </div>
                    </td>
                  </tr>
                  <tr>
                    <td>
                      <span data-toggle="tooltip" data-placement="bottom" title="Hardware">
                        Hardware
                      </span>                    </td>
                    <td>
                      <span data-toggle="tooltip" data-placement="bottom" title="Pendrive 16GB USB 2.0 JumpDrive S50">
                        Pendrive 16GB USB 2.0 JumpDrive S50
                      </span>
                      <p class="bottom">
                        PEN123112
                      </p>
                    </td>
                    <td>
                      <span class="label label-success">Vigente</span>
                    </td>
                    <td>
                      10
                    </td>
                    <td>
                      $5.400
                    </td>
                    <td>
                      $54.000
                    </td>
                    <td>
                      <div class="table-actions-toolbar">
                        <div class="btn-group">
                          <a class="btn btn-default btn-xs" data-toggle="tooltip" data-title="Ver tarjeta de existencia" data-container="body" href="/stock-actual/114/existencia">
                            <i class="fa fa-fw fa-bars"></i>
</a>
                          <a class="btn btn-default btn-xs" data-modal="stock-by-warehouses" data-url="/stock-actual/114/stock-por-bodega" data-toggle="tooltip" data-title="Ver disponibilidad por bodega" data-container="body" href="#">
                            <i class="fa fa-fw fa-map-marker"></i>
</a>                        </div>
                      </div>
                    </td>
                  </tr>
                  <tr>
                    <td>
                      <span data-toggle="tooltip" data-placement="bottom" title="Hardware">
                        Hardware
                      </span>                    </td>
                    <td>
                      <span data-toggle="tooltip" data-placement="bottom" title="SmartTV UJ6300 43&quot; LED Ultra HD 4K ">
                        SmartTV UJ6300 43&quot; LED Ultra HD 4K 
                      </span>
                      <p class="bottom">
                        54322
                      </p>
                    </td>
                    <td>
                      <span class="label label-success">Vigente</span>
                    </td>
                    <td>
                      9
                    </td>
                    <td>
                      $198.000
                    </td>
                    <td>
                      $1.782.000
                    </td>
                    <td>
                      <div class="table-actions-toolbar">
                        <div class="btn-group">
                          <a class="btn btn-default btn-xs" data-toggle="tooltip" data-title="Ver tarjeta de existencia" data-container="body" href="/stock-actual/116/existencia">
                            <i class="fa fa-fw fa-bars"></i>
</a>
                          <a class="btn btn-default btn-xs" data-modal="stock-by-warehouses" data-url="/stock-actual/116/stock-por-bodega" data-toggle="tooltip" data-title="Ver disponibilidad por bodega" data-container="body" href="#">
                            <i class="fa fa-fw fa-map-marker"></i>
</a>                        </div>
                      </div>
                    </td>
                  </tr>
                  <tr>
                    <td>
                      <span data-toggle="tooltip" data-placement="bottom" title="Hardware">
                        Hardware
                      </span>                    </td>
                    <td>
                      <span data-toggle="tooltip" data-placement="bottom" title="Adaptador Lightning a 30-pin">
                        Adaptador Lightning a 30-pin
                      </span>
                      <p class="bottom">
                        H-ALA3-335272
                      </p>
                    </td>
                    <td>
                      <span class="label label-success">Vigente</span>
                    </td>
                    <td>
                      7
                    </td>
                    <td>
                      $4.000
                    </td>
                    <td>
                      $28.000
                    </td>
                    <td>
                      <div class="table-actions-toolbar">
                        <div class="btn-group">
                          <a class="btn btn-default btn-xs" data-toggle="tooltip" data-title="Ver tarjeta de existencia" data-container="body" href="/stock-actual/571/existencia">
                            <i class="fa fa-fw fa-bars"></i>
</a>
                          <a class="btn btn-default btn-xs" data-modal="stock-by-warehouses" data-url="/stock-actual/571/stock-por-bodega" data-toggle="tooltip" data-title="Ver disponibilidad por bodega" data-container="body" href="#">
                            <i class="fa fa-fw fa-map-marker"></i>
</a>                        </div>
                      </div>
                    </td>
                  </tr>
                  <tr>
                    <td>
                      <span data-toggle="tooltip" data-placement="bottom" title="Hardware">
                        Hardware
                      </span>                    </td>
                    <td>
                      <span data-toggle="tooltip" data-placement="bottom" title="Pantalla LCD Touch 7">
                        Pantalla LCD Touch 7
                      </span>
                      <p class="bottom">
                        PT12322
                      </p>
                    </td>
                    <td>
                      <span class="label label-success">Vigente</span>
                    </td>
                    <td>
                      4
                    </td>
                    <td>
                      $35.000
                    </td>
                    <td>
                      $140.000
                    </td>
                    <td>
                      <div class="table-actions-toolbar">
                        <div class="btn-group">
                          <a class="btn btn-default btn-xs" data-toggle="tooltip" data-title="Ver tarjeta de existencia" data-container="body" href="/stock-actual/112/existencia">
                            <i class="fa fa-fw fa-bars"></i>
</a>
                          <a class="btn btn-default btn-xs" data-modal="stock-by-warehouses" data-url="/stock-actual/112/stock-por-bodega" data-toggle="tooltip" data-title="Ver disponibilidad por bodega" data-container="body" href="#">
                            <i class="fa fa-fw fa-map-marker"></i>
</a>                        </div>
                      </div>
                    </td>
                  </tr>
                  <tr>
                    <td>
                      <span data-toggle="tooltip" data-placement="bottom" title="Pantalones">
                        Pantalones
                      </span>                    </td>
                    <td>
                      <span data-toggle="tooltip" data-placement="bottom" title="pantalon rayas">
                        pantalon rayas
                      </span>
                      <p class="bottom">
                        P-PR-920711
                      </p>
                    </td>
                    <td>
                      <span class="label label-success">Vigente</span>
                    </td>
                    <td>
                      3
                    </td>
                    <td>
                      $17.000
                    </td>
                    <td>
                      $51.000
                    </td>
                    <td>
                      <div class="table-actions-toolbar">
                        <div class="btn-group">
                          <a class="btn btn-default btn-xs" data-toggle="tooltip" data-title="Ver tarjeta de existencia" data-container="body" href="/stock-actual/19564/existencia">
                            <i class="fa fa-fw fa-bars"></i>
</a>
                          <a class="btn btn-default btn-xs" data-modal="stock-by-warehouses" data-url="/stock-actual/19564/stock-por-bodega" data-toggle="tooltip" data-title="Ver disponibilidad por bodega" data-container="body" href="#">
                            <i class="fa fa-fw fa-map-marker"></i>
</a>                        </div>
                      </div>
                    </td>
                  </tr>
              </tbody>
          </table>

          <div id="operation-paginate" class="text-center">
              <nav class="pagination">
    
    
        <span class="page current">
  1
</span>

        <span class="page">
  <a rel="next" href="/stock-actual?page=2">2</a>
</span>

      <span class="next">
  <a rel="next" href="/stock-actual?page=2">Siguiente &rsaquo;</a>
</span>

      <span class="last">
  <a href="/stock-actual?page=2">Última &raquo;</a>
</span>

  </nav>

          </div>
        </div>

  </div>
</div>

      </div>
    </div>
  </div>
</div>
@endsection