@extends('layouts.app')

@section('helper')
  <ol class="bg-white shadow-box breadcrumb">
    <li><a href="https://demo.relbase.cl/">Inicio</a></li>
    <li><a href="https://demo.relbase.cl/stock-actual">Stock Actual</a></li>
    <li class="active">Tarjeta de existencia</li>
  </ol>
@endsection

@section('content')
<div class="row mt-10 mt-10">
  <div class="col-md-12">
    <div class="container-fluid bg-white shadow-box">
      <form id="form-filter" data-turboboost="true" action="/stock-actual/117/existencia" accept-charset="UTF-8" data-remote="true" method="get">
      	<input name="utf8" type="hidden" value="&#x2713;" />
        <div class="row mt-10">
          <div class="col-md-6">
            <div class="row">
              <div class="col-md-4">
                <label>Producto</label>
              </div>
              <div class="col-md-5">
                <label>
                  <strong>MO3321 Mouse MS-117C Óptico USB Negro</strong>
                </label>
              </div>
            </div>
            <div class="row">
              <div class="col-md-4">
                <label>Categoría</label>
              </div>
              <div class="col-md-5">
                <label><strong>Hardware</strong></label>
              </div>
            </div>
            <div class="row">
              <div class="col-md-4">
                <label>Stock disponible</label>
              </div>
              <div class="col-md-5">
                <label><strong>27</strong></label>
              </div>
            </div>
            <div class="row">
              <div class="col-md-4">
                <label>Valor inventario</label>
              </div>
              <div class="col-md-5">
                <label><strong>$269.730</strong></label>
              </div>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-md-2">
            <label>Bodega(s) </label>
          </div>
          <div class="col-md-4">
            <select name="ware_house_ids[]" id="ware_house_ids" multiple="multiple" class="select2 form-control"><option value="13">[Casa matriz] Bodega principal</option>
<option value="77">[Casa matriz] Bodega secundaria</option></select>
          </div>
          <div class="col-md-2 col-xs-12">
            <span class="filter-spinner"></span> <!-- Spinner de espera! -->
          </div>
          <div class="col-md-4 col-xs-12 text-right">
            <label class="mb-0">Descargar </label>
            <label class="mb-0">
              <a data-toggle="tooltip" data-placement="bottom" data-html="true" data-title="Descargar consulta en formato excel" href="/stock-actual/117/existencia.xls">
                Excel
</a>            </label>
          </div>
        </div>
</form>      <div class="row">
        <div class="col-sx-12 col-sm-12 col-lg-12 mt-20">
            <div id="operations-details">
              <div class="table-responsive">
  <table class="table">
    <thead>
      <tr>
        <th>Fecha</th>
        <th>Bodega</th>
        <th>Entrada</th>
        <th>Salida</th>
        <th>Stock</th>
        <th>Costo unitario</th>
        <th>Total</th>
        <th>Folio/Documento</th>
      </tr>
      <tbody>
          <tr>
            <td>
              24-05-2018
            </td>
            <td>
              <span data-toggle="tooltip" data-placement="bottom" title="Bodega principal">
                Bodega principal
              </span>
            </td>
            <td>
              100
            </td>
            <td>
              
            </td>
            <td>
              100
            </td>
            <td>
              $9.990
            </td>
            <td>
              $999.000
            </td>
            <td>
              983
            </td>
          </tr>
          <tr>
            <td>
              24-05-2018
            </td>
            <td>
              <span data-toggle="tooltip" data-placement="bottom" title="Bodega principal">
                Bodega principal
              </span>
            </td>
            <td>
              
            </td>
            <td>
              3
            </td>
            <td>
              97
            </td>
            <td>
              $9.990
            </td>
            <td>
              $29.970
            </td>
            <td>
              <a target="_blank" href="https://relke-erp-test.s3-us-west-2.amazonaws.com/uploads/6/6/invoice/2445/envio_2445_442019.pdf?X-Amz-Expires=1800&amp;X-Amz-Date=20190411T092202Z&amp;X-Amz-Algorithm=AWS4-HMAC-SHA256&amp;X-Amz-Credential=AKIAJDGF5OAMF5ED5Z5A/20190411/us-west-2/s3/aws4_request&amp;X-Amz-SignedHeaders=host&amp;X-Amz-Signature=b6cecd2f6951db0e870131d26240e199ec8241bafe7e69ee8eb718310b299bbc">FACT. ELECTRÓNICA: Nº 770</a>
            </td>
          </tr>
          <tr>
            <td>
              25-05-2018
            </td>
            <td>
              <span data-toggle="tooltip" data-placement="bottom" title="Bodega principal">
                Bodega principal
              </span>
            </td>
            <td>
              
            </td>
            <td>
              2
            </td>
            <td>
              95
            </td>
            <td>
              $9.990
            </td>
            <td>
              $19.980
            </td>
            <td>
              <a target="_blank" href="https://relke-erp-test.s3-us-west-2.amazonaws.com/uploads/6/6/receipt/2474/envio_2474_789319.pdf?X-Amz-Expires=1800&amp;X-Amz-Date=20190411T092202Z&amp;X-Amz-Algorithm=AWS4-HMAC-SHA256&amp;X-Amz-Credential=AKIAJDGF5OAMF5ED5Z5A/20190411/us-west-2/s3/aws4_request&amp;X-Amz-SignedHeaders=host&amp;X-Amz-Signature=36efd507ee3c91b0a063529f17348e2e711cd8f1779da89cb94fdb937af18c86">BOLETA ELECTRÓNICA: Nº 185</a>
            </td>
          </tr>
          <tr>
            <td>
              11-07-2018
            </td>
            <td>
              <span data-toggle="tooltip" data-placement="bottom" title="Bodega principal">
                Bodega principal
              </span>
            </td>
            <td>
              
            </td>
            <td>
              4
            </td>
            <td>
              91
            </td>
            <td>
              $9.990
            </td>
            <td>
              $39.960
            </td>
            <td>
              <a target="_blank" href="https://relke-erp-test.s3-us-west-2.amazonaws.com/uploads/6/6/invoice/3249/envio_3249_232495.pdf?X-Amz-Expires=1800&amp;X-Amz-Date=20190411T092202Z&amp;X-Amz-Algorithm=AWS4-HMAC-SHA256&amp;X-Amz-Credential=AKIAJDGF5OAMF5ED5Z5A/20190411/us-west-2/s3/aws4_request&amp;X-Amz-SignedHeaders=host&amp;X-Amz-Signature=73ae7446b14eea8e4044dee9514bc85200468da0bf7d732f0c12f5478b3ce5e1">FACT. ELECTRÓNICA: Nº 806</a>
            </td>
          </tr>
          <tr>
            <td>
              13-08-2018
            </td>
            <td>
              <span data-toggle="tooltip" data-placement="bottom" title="Bodega principal">
                Bodega principal
              </span>
            </td>
            <td>
              
            </td>
            <td>
              10
            </td>
            <td>
              81
            </td>
            <td>
              $9.990
            </td>
            <td>
              $99.900
            </td>
            <td>
              <a target="_blank" href="https://relke-erp-test.s3-us-west-2.amazonaws.com/uploads/6/6/receipt/3657/envio_3657_271500.pdf?X-Amz-Expires=1800&amp;X-Amz-Date=20190411T092202Z&amp;X-Amz-Algorithm=AWS4-HMAC-SHA256&amp;X-Amz-Credential=AKIAJDGF5OAMF5ED5Z5A/20190411/us-west-2/s3/aws4_request&amp;X-Amz-SignedHeaders=host&amp;X-Amz-Signature=501c9a705889193e6a9791bc4e58e8e4d5e295fbc893342d23d4cbbe25771d6f">BOLETA ELECTRÓNICA: Nº 195</a>
            </td>
          </tr>
          <tr>
            <td>
              11-09-2018
            </td>
            <td>
              <span data-toggle="tooltip" data-placement="bottom" title="Bodega principal">
                Bodega principal
              </span>
            </td>
            <td>
              
            </td>
            <td>
              10
            </td>
            <td>
              71
            </td>
            <td>
              $9.990
            </td>
            <td>
              $99.900
            </td>
            <td>
              <a target="_blank" href="https://relke-erp-test.s3-us-west-2.amazonaws.com/uploads/6/6/receipt/4338/envio_4338_711419.pdf?X-Amz-Expires=1800&amp;X-Amz-Date=20190411T092202Z&amp;X-Amz-Algorithm=AWS4-HMAC-SHA256&amp;X-Amz-Credential=AKIAJDGF5OAMF5ED5Z5A/20190411/us-west-2/s3/aws4_request&amp;X-Amz-SignedHeaders=host&amp;X-Amz-Signature=38bf23603cfacf7a975576fcf5fa8a34745e98a95ad2f732823d6c8fe737a4b4">BOLETA ELECTRÓNICA: Nº 200</a>
            </td>
          </tr>
          <tr>
            <td>
              11-09-2018
            </td>
            <td>
              <span data-toggle="tooltip" data-placement="bottom" title="Bodega principal">
                Bodega principal
              </span>
            </td>
            <td>
              
            </td>
            <td>
              10
            </td>
            <td>
              61
            </td>
            <td>
              $9.990
            </td>
            <td>
              $99.900
            </td>
            <td>
              <a target="_blank" href="https://relke-erp-test.s3-us-west-2.amazonaws.com/uploads/6/6/invoice/4340/envio_4340_716946.pdf?X-Amz-Expires=1800&amp;X-Amz-Date=20190411T092202Z&amp;X-Amz-Algorithm=AWS4-HMAC-SHA256&amp;X-Amz-Credential=AKIAJDGF5OAMF5ED5Z5A/20190411/us-west-2/s3/aws4_request&amp;X-Amz-SignedHeaders=host&amp;X-Amz-Signature=1dd2e899a65e28891dd057800162f57c97d7096a9a99efdaf4c97c84b8d4a014">FACT. ELECTRÓNICA: Nº 837</a>
            </td>
          </tr>
          <tr>
            <td>
              14-11-2018
            </td>
            <td>
              <span data-toggle="tooltip" data-placement="bottom" title="Bodega principal">
                Bodega principal
              </span>
            </td>
            <td>
              
            </td>
            <td>
              12
            </td>
            <td>
              49
            </td>
            <td>
              $9.990
            </td>
            <td>
              $119.880
            </td>
            <td>
              <a target="_blank" href="https://relke-erp-test.s3-us-west-2.amazonaws.com/uploads/6/6/receipt/5759/envio_5759_221015.pdf?X-Amz-Expires=1800&amp;X-Amz-Date=20190411T092202Z&amp;X-Amz-Algorithm=AWS4-HMAC-SHA256&amp;X-Amz-Credential=AKIAJDGF5OAMF5ED5Z5A/20190411/us-west-2/s3/aws4_request&amp;X-Amz-SignedHeaders=host&amp;X-Amz-Signature=e00ce3ba63ee4c297f92fb964f02acb6b54871bd5f29d97def90c50537812e70">BOLETA ELECTRÓNICA: Nº 412</a>
            </td>
          </tr>
          <tr>
            <td>
              02-12-2018
            </td>
            <td>
              <span data-toggle="tooltip" data-placement="bottom" title="Bodega principal">
                Bodega principal
              </span>
            </td>
            <td>
              
            </td>
            <td>
              10
            </td>
            <td>
              39
            </td>
            <td>
              $9.990
            </td>
            <td>
              $99.900
            </td>
            <td>
              <a target="_blank" href="https://relke-erp-test.s3-us-west-2.amazonaws.com/uploads/6/6/receipt/6121/envio_6121_705067.pdf?X-Amz-Expires=1800&amp;X-Amz-Date=20190411T092202Z&amp;X-Amz-Algorithm=AWS4-HMAC-SHA256&amp;X-Amz-Credential=AKIAJDGF5OAMF5ED5Z5A/20190411/us-west-2/s3/aws4_request&amp;X-Amz-SignedHeaders=host&amp;X-Amz-Signature=f89d397e1037c75bb158dbc13edcc9ea88101fd6bac9015fbbfb6fa96068605b">BOLETA ELECTRÓNICA: Nº 414</a>
            </td>
          </tr>
          <tr>
            <td>
              02-12-2018
            </td>
            <td>
              <span data-toggle="tooltip" data-placement="bottom" title="Bodega principal">
                Bodega principal
              </span>
            </td>
            <td>
              
            </td>
            <td>
              2
            </td>
            <td>
              37
            </td>
            <td>
              $9.990
            </td>
            <td>
              $19.980
            </td>
            <td>
              <a target="_blank" href="https://relke-erp-test.s3-us-west-2.amazonaws.com/uploads/6/6/receipt/6122/envio_6122_372323.pdf?X-Amz-Expires=1800&amp;X-Amz-Date=20190411T092202Z&amp;X-Amz-Algorithm=AWS4-HMAC-SHA256&amp;X-Amz-Credential=AKIAJDGF5OAMF5ED5Z5A/20190411/us-west-2/s3/aws4_request&amp;X-Amz-SignedHeaders=host&amp;X-Amz-Signature=8771444a809366d74895eb923ca8718d93b3c9cef83f270f00109a7b39051a8f">BOLETA ELECTRÓNICA: Nº 415</a>
            </td>
          </tr>
          <tr>
            <td>
              04-01-2019
            </td>
            <td>
              <span data-toggle="tooltip" data-placement="bottom" title="Bodega principal">
                Bodega principal
              </span>
            </td>
            <td>
              
            </td>
            <td>
              5
            </td>
            <td>
              32
            </td>
            <td>
              $9.990
            </td>
            <td>
              $49.950
            </td>
            <td>
              <a target="_blank" href="https://relke-erp-test.s3-us-west-2.amazonaws.com/uploads/6/6/receipt/7331/envio_7331_725931.pdf?X-Amz-Expires=1800&amp;X-Amz-Date=20190411T092202Z&amp;X-Amz-Algorithm=AWS4-HMAC-SHA256&amp;X-Amz-Credential=AKIAJDGF5OAMF5ED5Z5A/20190411/us-west-2/s3/aws4_request&amp;X-Amz-SignedHeaders=host&amp;X-Amz-Signature=62a702bca65f0b249ebf4b67c8081967bab7d913bf49f7620eb999a17df1b06c">BOLETA ELECTRÓNICA: Nº 418</a>
            </td>
          </tr>
          <tr>
            <td>
              05-04-2019
            </td>
            <td>
              <span data-toggle="tooltip" data-placement="bottom" title="Bodega principal">
                Bodega principal
              </span>
            </td>
            <td>
              
            </td>
            <td>
              5
            </td>
            <td>
              27
            </td>
            <td>
              $9.990
            </td>
            <td>
              $49.950
            </td>
            <td>
              <a target="_blank" href="https://relke-erp-test.s3-us-west-2.amazonaws.com/uploads/6/6/receipt/8132/envio_8132_523740.pdf?X-Amz-Expires=1800&amp;X-Amz-Date=20190411T092202Z&amp;X-Amz-Algorithm=AWS4-HMAC-SHA256&amp;X-Amz-Credential=AKIAJDGF5OAMF5ED5Z5A/20190411/us-west-2/s3/aws4_request&amp;X-Amz-SignedHeaders=host&amp;X-Amz-Signature=899002045674f55954d107611143ac267d05998baad8e8fb1a895a71c0060541">BOLETA ELECTRÓNICA: Nº 428</a>
            </td>
          </tr>
      </tbody>
  </table>
  <div id="operation-paginate" class="text-center">
  </div>
</div>

            </div>
        </div>
      </div>

    </div>
  </div>
</div>
@endsection