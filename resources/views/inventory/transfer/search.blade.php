@extends('layouts.app')

@section('content')
	<div class="container-fluid bg-white shadow-box">
    	<form id="form-transfer-warehouse-search" action="/transferencia-bodegas" accept-charset="UTF-8" method="get">
    		<input name="utf8" type="hidden" value="&#x2713;" />
	<div class="row mt-10">
		<div class="col-md-2 col-xs-12 text-left">
        <a class="btn btn-success btn-accion btn-sm" id="btn-new-transfer-warehouse" data-toggle="tooltip" data-placement="bottom" data-html="true" href="/transferencia-bodegas/new">
      		<i class="fa fa-plus"></i> Nuevo
</a>    </div>
	  <div class="col-md-offset-7 col-md-3 col-xs-12 text-right">
			<div class="form-search">
	    	<input type="text" name="query" id="navbar-search" class="form-control" placeholder="Buscar" data-toggle="tooltip" data-placement="bottom" data-title="Buscar por bodega" />
			</div>
		</div>
	</div>
		<div class="row">
			<div class="col-md-12 col-xs-12 text-right">
					<label>Descargar</label>
					<label>
						<a data-toggle="tooltip" data-placement="bottom" data-html="true" data-title="Descargar consulta &lt;br&gt;en formato &lt;br&gt;Excel." href="/transferencia-bodegas.xls">
							Excel
</a>					</label>
			</div>
		</div>
</form>
  <div class="row mt-10">
    <div class="col-md-12">
    	  <div id="warehouses-list">
      <div class="table-responsive">
        <table class="table table-hover">
          <thead>
            <tr>
              <th>Fecha transferencia</th>
              <th>Origen</th>
              <th>Destino</th>
              <th>Usuario</th>
              <th class="align-right">Acciones</th>
            </tr>
            <tbody>
              <tr id="transfer_warehouse-1">
                <td>
                  24-05-2018
                </td>
                <td>
                  Bodega principal
                </td>
                <td>
                  Bodega secundaria
                </td>
                <td>
                  Demo relBase
                </td>
                <td class="align-right width-150">
                  <div class="table-actions-toolbar width-150">
                    <div class="btn-group">
                        <a class="btn btn-default btn-xs" data-toggle="tooltip" data-title="Ver" data-container="body" href="/transferencia-bodegas/1">
                          <i class="fa fa-fw fa-eye"></i>
</a>                    </div>
                  </div>
                </td>
              </tr>
        </table>
      </div>
      <div id="transfer-warehouse-paginate" class="text-center">
       
      </div>
  </div>

    </div>
  </div>

@endsection