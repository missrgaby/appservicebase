@extends('layouts.app')

@section('content')
<div class="container-fluid bg-white shadow-box pt-20">
  <form id="transfer-warehouse-form" class="form-horizontal" enctype="multipart/form-data" action="/transferencia-bodegas" accept-charset="UTF-8" method="post"><input name="utf8" type="hidden" value="&#x2713;" />
    <div class="row">
      <div class="col-md-12 col-xs-12">
        <div class="page-header">
          <h5>Transferencia entre bodegas</h5>
        </div>
        <div class="row">
          <div class="col-md-6 col-xs-12">
            <div class="form-group">
              <div class="col-md-3">
                <label>Bodega origen <abbr data-toggle="tooltip" title="Campo obligatorio">*</abbr></label>
              </div>
              <div class="col-md-7">
                <select class="filter-select form-control js-transfer-ware-house-id" name="transfer_warehouse[ware_house_from_id]" id="transfer_warehouse_ware_house_from_id"><option value="">Seleccione...</option>
<option value="13">[Casa matriz] Bodega principal</option>
<option value="77">[Casa matriz] Bodega secundaria</option></select>
              </div>
              <div class="col-md-1">
                <span class="filter-spinner"></span> <!-- Spinner de espera! -->
              </div>
            </div>
            <div class="form-group">
              <div class="col-md-3">
                <label>Bodega destino <abbr data-toggle="tooltip" title="Campo obligatorio">*</abbr></label>
              </div>
              <div class="col-md-7">
                <select class="filter-select form-control" name="transfer_warehouse[ware_house_to_id]" id="transfer_warehouse_ware_house_to_id"><option value="">Seleccione...</option>
<option value="13">[Casa matriz] Bodega principal</option>
<option value="77">[Casa matriz] Bodega secundaria</option></select>
              </div>
            </div>
          </div>
          <div class="col-md-6 col-xs-12">
            <div class="form-group">
              <div class="col-md-3">
                <label>Fecha transferencia <abbr data-toggle="tooltip" title="Campo obligatorio">*</abbr></label>
              </div>
              <div class="col-md-4">
                <div class="input-group date input-date">
                  <input value="11-04-2019" class="form-control" type="text" name="transfer_warehouse[date_to]" id="transfer_warehouse_date_to" />
                  <div class="input-group-addon">
                      <i class="fa fa-calendar" aria-hidden="true"></i>
                  </div>
                </div>
              </div>
            </div>
            <div class="form-group">
              <div class="col-md-3">
                <label>Observación</label>
              </div>
              <div class="col-md-8">
                <textarea class="form-control" name="transfer_warehouse[note]" id="transfer_warehouse_note">
</textarea>
              </div>
            </div>
          </div>
        </div>

        <div class="row mt-10">
          <div class="col-md-12 col-xs-12 text-right">
            <div class="table-actions-toolbar">
              <div class="btn-group">
                <a class="item_transfer_warehouse_add_fields btn btn-success btn-xs btn-success item_transfer_warehouses_add_fields" data-id="69926299833720" data-fields="&lt;tr&gt;  &lt;td&gt;    &lt;input class=&quot;js-item-stock-destroy&quot; type=&quot;hidden&quot; value=&quot;false&quot; name=&quot;transfer_warehouse[item_transfer_warehouses_attributes][69926299833720][_destroy]&quot; id=&quot;transfer_warehouse_item_transfer_warehouses_attributes_69926299833720__destroy&quot; /&gt;    &lt;input value=&quot;6&quot; type=&quot;hidden&quot; name=&quot;transfer_warehouse[item_transfer_warehouses_attributes][69926299833720][company_id]&quot; id=&quot;transfer_warehouse_item_transfer_warehouses_attributes_69926299833720_company_id&quot; /&gt;    &lt;input value=&quot;6&quot; type=&quot;hidden&quot; name=&quot;transfer_warehouse[item_transfer_warehouses_attributes][69926299833720][business_id]&quot; id=&quot;transfer_warehouse_item_transfer_warehouses_attributes_69926299833720_business_id&quot; /&gt;    &lt;select class=&quot;filter-select-product-inventory form-control js-item-transfer-stock-product-id js-item-transfer-stock-product-id-69926299833720&quot; name=&quot;transfer_warehouse[item_transfer_warehouses_attributes][69926299833720][product_id]&quot; id=&quot;transfer_warehouse_item_transfer_warehouses_attributes_69926299833720_product_id&quot;&gt;&lt;option value=&quot;&quot;&gt;Seleccione...&lt;/option&gt;&lt;option value=&quot;571&quot;&gt;[H-ALA3-335272] Adaptador Lightning a 30-pin&lt;/option&gt;&lt;option value=&quot;121&quot;&gt;[03] Asesorías profesionales&lt;/option&gt;&lt;option value=&quot;572&quot;&gt;[C-CEAP-45173] Camisa en algodón premium&lt;/option&gt;&lt;option value=&quot;13568&quot;&gt;[01] Capacitación y Asesoría &lt;/option&gt;&lt;option value=&quot;12213&quot;&gt;[] Corrige Dato Receptor&lt;/option&gt;&lt;option value=&quot;115&quot;&gt;[14554] Disco Externo 2.5&amp;quot; USB 3.0&lt;/option&gt;&lt;option value=&quot;118&quot;&gt;[234234] Epson Scanner Perfection V19&lt;/option&gt;&lt;option value=&quot;122&quot;&gt;[11312] Licencia Win10 xp 2017&lt;/option&gt;&lt;option value=&quot;120&quot;&gt;[02] Mantención de aplicaciones web&lt;/option&gt;&lt;option value=&quot;19506&quot;&gt;[SC-M-962251] Marraquetas&lt;/option&gt;&lt;option value=&quot;117&quot;&gt;[MO3321] Mouse MS-117C Óptico USB Negro&lt;/option&gt;&lt;option value=&quot;113&quot;&gt;[24825] Multifuncional Laser B&amp;amp;N WiFi/Ethernet &lt;/option&gt;&lt;option value=&quot;112&quot;&gt;[PT12322] Pantalla LCD Touch 7&lt;/option&gt;&lt;option value=&quot;19564&quot;&gt;[P-PR-920711] pantalon rayas&lt;/option&gt;&lt;option value=&quot;19566&quot;&gt;[P-PR-PRB-149490] pantalon rayas pantalon rayas blancas&lt;/option&gt;&lt;option value=&quot;19565&quot;&gt;[P-PR-PRN-411544] pantalon rayas pantalon rayas negras&lt;/option&gt;&lt;option value=&quot;19567&quot;&gt;[P-PR-PRR-540227] pantalon rayas pantalon rayas rojas&lt;/option&gt;&lt;option value=&quot;114&quot;&gt;[PEN123112] Pendrive 16GB USB 2.0 JumpDrive S50&lt;/option&gt;&lt;option value=&quot;116&quot;&gt;[54322] SmartTV UJ6300 43&amp;quot; LED Ultra HD 4K &lt;/option&gt;&lt;option value=&quot;569&quot;&gt;[518342] Zapatilla Agility Peak Flex Talla 41&lt;/option&gt;&lt;option value=&quot;570&quot;&gt;[584030] Zapatilla Agility Peak Flex Talla 42&lt;/option&gt;&lt;option value=&quot;575&quot;&gt;[C-CEAP-TL-155728] Camisa en algodón premium Talla L&lt;/option&gt;&lt;option value=&quot;574&quot;&gt;[C-CEAP-TM-675108] Camisa en algodón premium Talla M&lt;/option&gt;&lt;option value=&quot;573&quot;&gt;[C-CEAP-TS-738937] Camisa en algodón premium Talla S&lt;/option&gt;&lt;option value=&quot;576&quot;&gt;[C-CEAP-TX-925068] Camisa en algodón premium Talla XL&lt;/option&gt;&lt;option value=&quot;568&quot;&gt;[425085] Zapatilla Agility Peak Flex&lt;/option&gt;&lt;/select&gt;  &lt;/td&gt;  &lt;td&gt;    &lt;p class=&quot;text-default js-item-transfer-stock-total js-item-transfer-stock-total-69926299833720&quot;&gt;          &lt;/p&gt;  &lt;/td&gt;  &lt;td&gt;&lt;input class=&quot;form-control js-item-transfer-stock-quantity js-item-transfer-stock-quantity-69926299833720&quot; step=&quot;any&quot; min=&quot;0&quot; type=&quot;number&quot; name=&quot;transfer_warehouse[item_transfer_warehouses_attributes][69926299833720][quantity]&quot; id=&quot;transfer_warehouse_item_transfer_warehouses_attributes_69926299833720_quantity&quot; /&gt;&lt;/td&gt;  &lt;td&gt;    &lt;div class=&quot;table-actions-toolbar&quot;&gt;      &lt;div class=&quot;btn-group&quot;&gt;        &lt;a class=&quot;btn btn-default btn-xs item_transfer_warehouses_remove_fields&quot; data-toggle=&quot;tooltip&quot; data-placement=&quot;top&quot; data-title=&quot;Borrar&quot; data-container=&quot;body&quot; href=&quot;#&quot;&gt;          &lt;i class=&quot;fa fa-trash&quot;&gt;&lt;/i&gt;&lt;/a&gt;      &lt;/div&gt;    &lt;/div&gt;  &lt;/td&gt;&lt;/tr&gt;" href="#"><i class="fa fa-plus"></i> Agregar producto</a>              </div>
            </div>
          </div>
        </div>

        <div class="row mt-20">
          <div class="col-md-12 col-xs-12">
            <div class="table-responsive">
              <table class="table" id="item_transfer_warehouse_table">
                <thead>
                  <tr>
                    <th width="50%">Producto</th>
                    <th width="15%">Cantidad inicial</th>
                    <th width="15%">Cantidad a transferir</th>
                    <th width="20%"></th>
                  </tr>
                </thead>
                <tbody>
                  
                    <tr>
  <td>
    <input class="js-item-stock-destroy" type="hidden" value="false" name="transfer_warehouse[item_transfer_warehouses_attributes][0][_destroy]" id="transfer_warehouse_item_transfer_warehouses_attributes_0__destroy" />
    <input value="6" type="hidden" name="transfer_warehouse[item_transfer_warehouses_attributes][0][company_id]" id="transfer_warehouse_item_transfer_warehouses_attributes_0_company_id" />
    <input value="6" type="hidden" name="transfer_warehouse[item_transfer_warehouses_attributes][0][business_id]" id="transfer_warehouse_item_transfer_warehouses_attributes_0_business_id" />
    <select class="filter-select-product-inventory form-control js-item-transfer-stock-product-id js-item-transfer-stock-product-id-0" name="transfer_warehouse[item_transfer_warehouses_attributes][0][product_id]" id="transfer_warehouse_item_transfer_warehouses_attributes_0_product_id"><option value="">Seleccione...</option>
<option value="571">[H-ALA3-335272] Adaptador Lightning a 30-pin</option>
<option value="121">[03] Asesorías profesionales</option>
<option value="572">[C-CEAP-45173] Camisa en algodón premium</option>
<option value="13568">[01] Capacitación y Asesoría </option>
<option value="12213">[] Corrige Dato Receptor</option>
<option value="115">[14554] Disco Externo 2.5&quot; USB 3.0</option>
<option value="118">[234234] Epson Scanner Perfection V19</option>
<option value="122">[11312] Licencia Win10 xp 2017</option>
<option value="120">[02] Mantención de aplicaciones web</option>
<option value="19506">[SC-M-962251] Marraquetas</option>
<option value="117">[MO3321] Mouse MS-117C Óptico USB Negro</option>
<option value="113">[24825] Multifuncional Laser B&amp;N WiFi/Ethernet </option>
<option value="112">[PT12322] Pantalla LCD Touch 7</option>
<option value="19564">[P-PR-920711] pantalon rayas</option>
<option value="19566">[P-PR-PRB-149490] pantalon rayas pantalon rayas blancas</option>
<option value="19565">[P-PR-PRN-411544] pantalon rayas pantalon rayas negras</option>
<option value="19567">[P-PR-PRR-540227] pantalon rayas pantalon rayas rojas</option>
<option value="114">[PEN123112] Pendrive 16GB USB 2.0 JumpDrive S50</option>
<option value="116">[54322] SmartTV UJ6300 43&quot; LED Ultra HD 4K </option>
<option value="569">[518342] Zapatilla Agility Peak Flex Talla 41</option>
<option value="570">[584030] Zapatilla Agility Peak Flex Talla 42</option>
<option value="575">[C-CEAP-TL-155728] Camisa en algodón premium Talla L</option>
<option value="574">[C-CEAP-TM-675108] Camisa en algodón premium Talla M</option>
<option value="573">[C-CEAP-TS-738937] Camisa en algodón premium Talla S</option>
<option value="576">[C-CEAP-TX-925068] Camisa en algodón premium Talla XL</option>
<option value="568">[425085] Zapatilla Agility Peak Flex</option></select>
  </td>
  <td>
    <p class="text-default js-item-transfer-stock-total js-item-transfer-stock-total-0">
      
    </p>
  </td>
  <td><input class="form-control js-item-transfer-stock-quantity js-item-transfer-stock-quantity-0" step="any" min="0" type="number" name="transfer_warehouse[item_transfer_warehouses_attributes][0][quantity]" id="transfer_warehouse_item_transfer_warehouses_attributes_0_quantity" /></td>
  <td>
    <div class="table-actions-toolbar">
      <div class="btn-group">
        <a class="btn btn-default btn-xs item_transfer_warehouses_remove_fields" data-toggle="tooltip" data-placement="top" data-title="Borrar" data-container="body" href="#">
          <i class="fa fa-trash"></i>
</a>      </div>
    </div>
  </td>
</tr>

                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="row mt-10">
      <div class="col-md-12 text-right mb-20">
        <a class="btn btn-link" href="/transferencia-bodegas">Cancelar</a>
      </div>
    </div>
</form></div>
@endsection