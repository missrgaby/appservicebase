<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
  <head>
    <title>App Services</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- CSS -->
    <link rel="stylesheet" media="all" href="/css/appsv.css" />
    <!-- Javascript -->
    <script src="/js/appsv.js"></script>
    <script>
    //<![CDATA[
          NProgressSetup.init();
    //]]>
    </script>    
  </head>
  <body class="bg-default">

    <div class="bg-parallax bg-back"></div>
    <div class="bg-parallax bg-front"></div>
    <div class="bg-parallax bg-middle"></div>

    <div id="content">
      <div class="navbar-trial navbar-inverse navbar-fixed-top" role="navigation">
      <div class="container">
        <div class="text-center color-black">
          <p>
            Mensaje
          </p>
        </div>
      </div>
      </div>

      <nav class="navbar navbar-default second-navbar">
        <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#">
            <img class="image-logo-front" src="/assets/front/logos/relbase-blanco-c50041a087dc3147d2453bfa500dbd4d5b2796cba34043fcd06400cfceb2ec45.png" alt="Demo" />
          </a>    
        </div>

        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
          <ul class="nav navbar-nav">
            <li class="">
              <a href="#">
                <i class="fa fa-id-card-o fa-2x"></i>
                <p>Clientes</p>
              </a>
            </li>
{{--             <li class="">
              <a href="#">
                <i class="fa fa-paper-plane-o fa-2x"></i>
                <p>Ventas</p>
              </a>
            </li> --}}
            <li class="">
                <a href="/">
                  <i class="fa fa-calculator fa-2x"></i>
                  <p>Punto de venta</p>
                </a>  
            </li>
{{--             <li class="">
              <a href="#">
                <i class="fa fa-file-text-o fa-2x"></i>
                <p>Compras</p>
              </a>
            </li>
            <li class="">
              <a href="#">
                <i class="fa fa-thumb-tack fa-2x"></i>
                <p>Recaudación</p>
              </a>
            </li> --}}
{{--             <li class="">
              <a href="#">
                <i class="fa fa-calendar fa-2x"></i>
                <p>Eventos</p>
              </a>
            </li> --}}
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                <i class="fa fa-cubes fa-2x"></i>
                <p>Inventario <strong class="caret"></strong></p>
              </a>
              <ul class="dropdown-menu">
                <li>
                  <a href="#">
                    <i class="fa fa-cubes"></i> Movimientos
                  </a>
                </li>
                <li>
                  <a href="/stock-actual">
                    <i class="fa fa-cube"></i> Stock Actual
                  </a>
                </li>
                <li>
                  <a href="/transferencia-bodegas">
                    <i class="fa fa-exchange"></i> Transferencia
                  </a>
                </li>
                <li>
                  <a href="/bodegas">
                    <i class="fa fa-archive"></i> Bodegas
                  </a>
                </li>
              </ul>              
            </li>
{{--             <li class="">
              <a href="#">
                <i class="fa fa-bar-chart fa-2x"></i>
                <p>Reportes</p>
              </a>
            </li> --}}
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                <i class="fa fa-cogs fa-2x"></i>
                <p>Configuración <strong class="caret"></strong></p>
              </a>
              <ul class="dropdown-menu">
{{--                 <li>
                  <a href="#">
                    <i class="fa fa-building"></i> Proveedores
                  </a>
                </li> --}}
                <li>
                  <a href="#">
                    <i class="fa fa-tags"></i> Categorías
                  </a>
                </li>
                <li>
                  <a href="#">
                    <i class="fa fa-tasks"></i> Productos
                  </a>
                </li>
                <li>
                  <a href="#">
                    <i class="fa fa-users"></i> Usuarios
                  </a>
                </li>
                <li role="separator" class="divider"></li>
                <li>
                  <a href="/canal">
                    <i class="fa fa-angle-double-right"></i> Más opciones ...
                  </a>    
                </li>
              </ul>
            </li>
          </ul>
          <ul class="nav navbar-nav navbar-right">
            <li class="dropdown">
              <a href="#" class="dropdown-toggle dropdown-toggle-profile" data-toggle="dropdown" role="button" aria-expanded="false">
                <i class="fa fa-user-circle fa-2x"></i>
                <p>Gabriela <span class="caret"></span></p>
              </a>
              <ul class="dropdown-menu" role="menu">
                <li class="">
                  <a href="#">
                    <i class="fa fa-user-circle"></i> Perfil
                  </a>
                </li>
                <li>
                  <a rel="nofollow" data-method="delete" href="/salir">
                    <i class="fa fa-sign-out"></i> Salir
                  </a>
                </li>
              </ul>
            </li>
          </ul>
        </div><!-- /.navbar-collapse -->
      </div><!-- /.container-fluid -->
    </nav>

    <div class="container-fluid">
      
      <div id="msg-div" class="mb-10">
        <div id="above-msg" style="width:100%">
        </div>
        <script type="text/javascript"> setTimeout(function(){ $("#above-msg").hide(1000); }, 10000); </script>
      </div>

      @yield('helper')

      @yield('content')

    </div>
    
  </div>
</div>

        <!-- Footer -->
        <div class="row mt-20">
          <div class="col-xs-12 col-sm-12 col-md-12">
          <span>
            <i class="fa fa-copyright"></i>
            <small> 2019 <a target="_blank" href="#">Relke</a></small>
            | <small class="text-right"><a target="_blank" href="#">Términos y Condiciones de Uso</a></small>
            | <small class="text-right"><a target="_blank" href="#">Política de Privacidad</a></small>
            | <small class="text-right"><a target="_blank" href="#">Centro de Ayuda</a></small>
          </span>
          </div>
        </div>
      </div>
    </div>
    
    <script>
      //<![CDATA[
      window.gon={};
      //]]>
    </script>
    <script>
    //<![CDATA[
          BootstrapSetup.init();
          RetinaSetup.init();
          Select2Setup.init();

          DeleterApp.init();
          InputMask.init();

          BootstrapSubmenu.init();
          BootstrapDatePickerSetup.init();

          ZendeskSetup.init('Gabriela A','gabriela.acurio.work@gmail.com');
    //]]>
    </script>    
  </body>
</html>
