<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInventoryStockDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('inventory_stock_details', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('inventory_stock_id');
            $table->unsignedInteger('inventory_transaction_detail_id');
            $table->unsignedInteger('warehouse_id');
            $table->integer('quantityInOut');
            $table->double('valuePerUnit', 14, 2);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('inventory_stock_details');
    }
}
