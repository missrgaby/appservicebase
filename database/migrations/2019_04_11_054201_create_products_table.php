<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('company_id');
            $table->enum('type', ['product', 'service']);
            $table->boolean('stockControl');
            $table->integer('taxIVA');
            $table->integer('taxICE');
            $table->string('code')->unique();
            $table->string('name');
            $table->text('description')->nullable();;
            $table->string('codebar')->unique();
            $table->unsignedInteger('category_id');
            $table->double('valuePerUnit', 14, 2);
            $table->double('price1', 14, 2);
            $table->double('price2', 14, 2);
            $table->double('price3', 14, 2);
            $table->boolean('priceManual');
            $table->integer('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
