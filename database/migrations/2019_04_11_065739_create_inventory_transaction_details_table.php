<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInventoryTransactionDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('inventory_transaction_details', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('inventory_transaction_id');
            $table->unsignedInteger('product_id');
            $table->integer('quantity');
            $table->double('valuePerUnit', 14, 2)->nullable();
            $table->double('total', 14, 2)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('inventory_transaction_details');
    }
}
