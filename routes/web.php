<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/bodegas', function () {
    return view('inventory.warehouse.search');
});

Route::get('/bodegas/new', function () {
    return view('inventory.warehouse.new');
});

Route::get('/transferencia-bodegas', function () {
    return view('inventory.transfer.search');
});

Route::get('/transferencia-bodegas/new', function () {
    return view('inventory.transfer.new');
});

Route::get('/stock-actual', function () {
    return view('inventory.stock.search');
});

Route::get('/stock-actual/{id}/existencia', function () {
    return view('inventory.stock.detail');
});





// Authentication Routes...
Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('login', 'Auth\LoginController@login');
Route::post('logout', 'Auth\LoginController@logout')->name('logout');

// Registration Routes...
//Route::get('register', 'Auth\RegisterController@showRegistrationForm')->name('register');
//Route::post('register', 'Auth\RegisterController@register');

// Password Reset Routes...
Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
Route::post('password/reset', 'Auth\ResetPasswordController@reset');